/**
 * \file sys.c
 *
 * \brief Main system routines implementation
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 * $Id: sys.c 9267 2014-03-18 21:46:19Z ataradov $
 *
 */

/*- Includes ---------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

#include "sysConfig.h"
//#include "nwk.h"
#include "hal.h"
#include "sys.h"
#include "sysclk.h"
#include "sysTimer.h"
#include "halEeprom.h"
#include "halUart.h"
#include "sysSettings.h"
#include "trace.h"

/*- Implementations --------------------------------------------------------*/

int SYS_PrintChar(char character, FILE *stream)
{
  UNUSED(stream);
  HAL_UartWriteByte(character);
  return 0;
}


FILE uart_str;

/*************************************************************************//**
*****************************************************************************/
void SYS_Init(void)
{
  fdev_setup_stream(&uart_str, SYS_PrintChar, NULL, _FDEV_SETUP_RW);

  HAL_Init();

	switch (sysclk_get_cpu_hz())
	{
		case 16000000ul:
		{
			HAL_UartInit(38400);
			break;
		}
		case 8000000ul:
		{
			HAL_UartInit(19200);
			break;
		}
		case 4000000ul:
		{
			HAL_UartInit(19200);
			break;
		}
		case 2000000ul:
		{
			HAL_UartInit(4800);
			break;
		}
		case 1000000ul:
		{
			HAL_UartInit(4800);
			break;
		}
		default:
		{
			TRACE_FATAL("Unknown clock frequencu %ld\n", sysclk_get_cpu_hz());
		}

	}
  stdout = &uart_str;
  uint32_t kHz = sysclk_get_cpu_hz() / 1000l;
  TRACE_USER("UART STARTED, clock frequency %lu.%03lu MHz\n", (uint32_t)(kHz / 1000), (uint32_t)(kHz % 1000));

  HAL_EepromInit();
  SYS_SettingsInit();

  SYS_TimerInit();
//  PHY_Init();
//  NWK_Init();
}

/*************************************************************************//**
*****************************************************************************/
void SYS_TaskHandler(void)
{
//  PHY_TaskHandler();
//  NWK_TaskHandler();
  SYS_TimerTaskHandler();
}
