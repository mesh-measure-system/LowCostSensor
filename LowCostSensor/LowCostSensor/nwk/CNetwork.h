/*
 * CNetwork.h
 *
 *  Created on: 1 paz 2014
 *      Author: Przemek
 */

#ifndef CNETWORK_H_
#define CNETWORK_H_

#include "CRun.h"
#include "CSettingsMgr.h"
#include "sysTimer.h"

#define CNET_DEFAULT_BEACON_ENDPOINT_ID   1
#define CNET_DEFAULT_BEACON_HANDLE        1

#define CNET_FH_CTRL_NO_COMMAND      0x00
#define CNET_FH_CTRL_BREAK_COMMAND   0x01
#define CNET_FH_CTRL_END_COMMAND     0x02
#define CNET_FH_CTRL_ASCII_COMMAND   0x04

typedef struct
{
  uint8_t				dstEndPoint;
  uint8_t				srcEndPoint;
  uint8_t       control;
}cnet_frame_header_t;


uint8_t usr_mlme_beacon_parse_ind(uint8_t* msdu_payload, uint8_t sduLength, uint8_t* msdu_beacon_response, uint8_t resp_max_len);

class CNetwork : public CRun, CTimerFired
{
  typedef enum {CNET_SLEEP, CNET_ACTIVE} CNetState;

  private:
    CSetting<uint32_t, SETTING_UINT32> mac_hi;
    CSetting<uint32_t, SETTING_UINT32> mac_lo;

    SYS_Timer_t sleepTimer;
    CNetState   netState;

  public:

    void Init();
    virtual void run();
    virtual bool isIdle();

    virtual void timerFired(struct SYS_Timer_t* timer);

    virtual const char* getClassName() {return __PRETTY_FUNCTION__;}

    virtual ~CNetwork() {};
};

extern CNetwork nwk;

#endif /* CNETWORK_H_ */
