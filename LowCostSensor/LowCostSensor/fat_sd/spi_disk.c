/*
 * spi_disk.c
 *
 * Created: 2014-11-29 16:35:08
 *  Author: Przemek
 */

#include "spi_disk.h"
#include "at45dbx.h"
#include <avr/wdt.h>

#ifdef TRACE_LEVEL
#undef TRACE_LEVEL
#endif // TRACE_LEVEL

#define TRACE_LEVEL TRACE_LEVEL_WARNING

#include "trace.h"

static volatile DSTATUS Stat = STA_NOINIT; /* Disk status */

DSTATUS SPI_disk_initialize(void)
{
  if (at45dbx_init() == true)
  {
    // disk initialized successfully
    if (at45dbx_mem_check() == true)
    {
      TRACE_USER("at45dbx_mem_check() == true\n");
    }
    Stat &= ~STA_NOINIT;
  }
  else
  {
    Stat |= STA_NOINIT;
  }
  return Stat;
}

DSTATUS SPI_disk_status(void)
{
  return Stat;
}

/**
  BYTE *buff,   Pointer to the data buffer to store read data
  DWORD sector, Start sector number (LBA)
  UINT count    Sector count (1..128) */
DRESULT SPI_disk_read(BYTE *buff, DWORD sector, UINT count)
{
  DRESULT result;
  bool  status;
  status = at45dbx_read_sector_open(sector);
  while (count && status)
  {
    count--;
    status = at45dbx_read_sector_to_ram(buff);
    buff += AT45DBX_SECTOR_SIZE;
  }
  at45dbx_read_close();

  if (status)
  {
    wdt_reset();
    result = RES_OK;
  }
  else
  {
    TRACE_USER("status = %d\n", (int)status);
    result = RES_ERROR;
  }
  return result;
}


/**
  const BYTE *buff, Pointer to the data to be written
  DWORD sector,     Start sector number (LBA)
  UINT count        Sector count (1..128)
  */
DRESULT SPI_disk_write(const BYTE *buff, DWORD sector, UINT count)
{
  DRESULT result;
  bool  status;
  status = at45dbx_write_sector_open(sector);
  while (count && status)
  {
    count--;
    status = at45dbx_write_sector_from_ram(buff);
    buff += AT45DBX_SECTOR_SIZE;
  }
  at45dbx_write_close();

  if (status)
  {
    wdt_reset();
    result = RES_OK;
  }
  else
  {
    TRACE_USER("status = %d\n", (int)status);
    result = RES_ERROR;
  }
  return result;
}

/**
  BYTE cmd,   Control code
  void *buff  Buffer to send/receive control data */
DRESULT SPI_disk_ioctl(BYTE cmd, void *buff)
{
  DRESULT res = RES_PARERR;
  switch (cmd)
  {
    case CTRL_SYNC:
    {
      /************************************************************************/
      /* Make sure that the device has finished pending write process.
         If the disk I/O module has a write back cache, the dirty buffers
         must be written back to the media immediately.
         Nothing to do for this command if each write operation to the media
         is completed within the disk_write() function.                                                                     */
      /************************************************************************/
      res = RES_OK;
      break;
    }
    case GET_SECTOR_COUNT: /* Get number of sectors on the disk (DWORD) */
    {
      DWORD count = (((uint32_t)1<<AT45DBX_MEM_SIZE)*AT45DBX_MEM_CNT) / AT45DBX_SECTOR_SIZE;
      *(DWORD*) buff = count;
      TRACE_USER("GET_SECTOR_COUNT = %d\n", (int)count);
      res = RES_OK;
      break;
    }
    case GET_BLOCK_SIZE: /* Get erase block size in unit of sector (DWORD) */
    {
      *(DWORD*) buff = AT45DBX_SECTOR_SIZE;
      TRACE_USER("GET_BLOCK_SIZE = %d\n", (int)AT45DBX_SECTOR_SIZE);
      res = RES_OK;
      break;
    }
    default:
    {
      TRACE_ERROR("SPI_disk_ioctl CMD not implemented cmd=%d, buff=0x%04x\n", cmd, buff);
      break;
    }

  }
  return res;
}
