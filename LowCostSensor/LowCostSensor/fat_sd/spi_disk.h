/*
 * spi_disk.h
 *
 * Created: 2014-11-29 16:30:35
 *  Author: Przemek
 */


#ifndef SPI_DISK_H_
#define SPI_DISK_H_

#include "diskio.h"

#ifdef __cplusplus
extern "C" {
#endif

DSTATUS SPI_disk_initialize(void);

DSTATUS SPI_disk_status(void);

/**
  BYTE *buff,   Pointer to the data buffer to store read data
  DWORD sector, Start sector number (LBA)
  UINT count    Sector count (1..128) */
DRESULT SPI_disk_read(BYTE *buff, DWORD sector, UINT count);


/**
  const BYTE *buff, Pointer to the data to be written
  DWORD sector,     Start sector number (LBA)
  UINT count        Sector count (1..128) */
DRESULT SPI_disk_write(const BYTE *buff, DWORD sector, UINT count);

/**
  BYTE cmd,   Control code
  void *buff  Buffer to send/receive control data */
DRESULT SPI_disk_ioctl(BYTE cmd, void *buff);


#ifdef __cplusplus
}
#endif


#endif /* SPI_DISK_H_ */