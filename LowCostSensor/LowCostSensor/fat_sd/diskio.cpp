/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2013        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various existing      */
/* storage control module to the FatFs module with a defined API.        */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
#include "mmc_avr.h"
#include "spi_disk.h"
#include "at45dbx.h"
#include "time.h"

#ifdef TRACE_LEVEL
#undef TRACE_LEVEL
#endif // TRACE_LEVEL

#define TRACE_LEVEL TRACE_LEVEL_WARNING
#include "trace.h"

/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(BYTE pdrv /* Physical drive number (0..) */
)
{
  DSTATUS result = STA_NOINIT;

  switch (pdrv)
  {
    case SPI:
    {
      result = SPI_disk_status();
      return result;
    }
    case MMC:
    {
//      result = MMC_disk_status();
      return result;
    }
  }
  return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Initialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(BYTE pdrv /* Physical drive number (0..) */)
{
  DSTATUS result = STA_NOINIT;

  TRACE_USER("disk init id=%d\n", (int) pdrv);
  switch (pdrv)
  {
    case SPI:
    {
      result = SPI_disk_initialize();
      TRACE_USER("disk init id=%d, result = %d\n", (int) pdrv, (int)result);
      break;
    }
    case MMC:
    {
//      result = MMC_disk_initialize();
      TRACE_USER("disk init id=%d, result = %d\n", (int) pdrv, (int)result);
      break;
    }
  }
  return result;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Sector address in LBA */
	UINT count		/* Number of sectors to read */
)
{
  DRESULT result = RES_PARERR;

  TRACE_USER("disk_read pdrv=%d, buff=0x%04x, sector=%lu, count=%u\n", (int)pdrv, buff, sector, (int)count);
  switch (pdrv)
  {
    case SPI:
    {
      wdt_reset();
      return SPI_disk_read(buff, sector, count);
      break;
    }
    case MMC:
    {
//      result = MMC_disk_read(buff, sector, count);
      return result;
    }
  }
  return RES_PARERR;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Sector address in LBA */
	UINT count			/* Number of sectors to write */
)
{
  DRESULT result = RES_PARERR;

  TRACE_USER("disk_write pdrv=%d, buff=0x%04x, sector=%lu, count=%u\n", (int)pdrv, buff, sector, (int)count);
  switch (pdrv)
  {
    case SPI:
    {
      wdt_reset();
      result = SPI_disk_write(buff, sector, count);
      break;
    }
    case MMC:
    {
//      result = MMC_disk_write(buff, sector, count);
      break;
    }
  }
  return result;
}
#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
  DRESULT result = RES_PARERR;
  switch (pdrv)
  {
    case SPI:
    {
      result = SPI_disk_ioctl(cmd, buff);
      return result;
    }
    case MMC:
    {
//      result = MMC_disk_ioctl(cmd, buff);
      return result;
    }
  }
  return RES_PARERR;
}
#endif

DWORD get_fattime (void)
{
  time_t curtime;
  struct tm *loctime;

  /* Get the current time. */
  curtime = time (NULL);

  /* Convert it to local time representation. */
  loctime = localtime(&curtime);

  /* Returns current time packed into a DWORD variable */
  return
        ((DWORD)(loctime->tm_year + 1900 - 1980) << 25)   /* Year */
      | ((DWORD)(loctime->tm_mon + 1) << 21)              /* Month */
      | ((DWORD)(loctime->tm_mday) << 16)                 /* Mday  */
      | ((DWORD)(loctime->tm_hour) << 11)                 /* Hour */
      | ((DWORD)(loctime->tm_min) << 5)                   /* Min */
      | ((DWORD)(loctime->tm_sec / 2));                   /* Sec */
}

