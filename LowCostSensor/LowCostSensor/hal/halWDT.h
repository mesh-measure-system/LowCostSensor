/*
* halWDT.h
*
* Created: 2014-11-24 20:22:09
* Author: Przemek
*/

#ifndef __HAL_WDT_H__
#define __HAL_WDT_H__

#include <avr/wdt.h>
#include "mega_reset_cause.h"

extern volatile reset_cause_t	reset_cause;

#define wdt_enable_interrupt(value)     \
__asm__ __volatile__ (                  \
"in __tmp_reg__,__SREG__" "\n\t"    \
"cli" "\n\t"    \
"wdr" "\n\t"    \
"sts %0,%1" "\n\t"  \
"out __SREG__,__tmp_reg__" "\n\t"   \
"sts %0,%2" "\n\t" \
: /* no outputs */  \
: "M" (_SFR_MEM_ADDR(_WD_CONTROL_REG)), \
"r" (_BV(_WD_CHANGE_BIT) | _BV(WDE)), \
"r" ((uint8_t) ((value & 0x08 ? _WD_PS3_MASK : 0x00) | _BV(WDE) | _BV(WDIE) | (value & 0x07)) ) \
: "r0"  \
)


#endif