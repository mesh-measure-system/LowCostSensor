/*
 * halT2RTC.h
 *
 *  Created on: 25 wrz 2014
 *      Author: Przemek
 */

#ifndef HALT2RTC_H_
#define HALT2RTC_H_

#include <compiler.h>

#include "time.h"

#ifdef __cplusplus
extern "C"
{
  #endif

extern volatile time_t __system_time;
extern volatile uint8_t last_timer_val;
extern volatile uint8_t called_non_isr;

//
// Initializes RTC timer based on the T2 timer and external 32.768kHz oscillator
void      HAL_T2RTCInit(void);

// function called from set_system_time.c
void HAL_T2RTCUpdateSysTime(void);

#ifdef __cplusplus
}
#endif

#endif /* HALT2RTC_H_ */
