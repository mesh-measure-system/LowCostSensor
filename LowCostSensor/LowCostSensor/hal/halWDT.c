/*
* halWDT.c
*
* Created: 2014-11-24 20:22:09
* Author: Przemek
*/

#include "halWDT.h"

#include <stdio.h>
#include <string.h>
#include "halUart.h"

#include "trace.h"

#if defined (__GNUC__)
volatile reset_cause_t	reset_cause __attribute__ ((section(".noinit")));
volatile uint16_t	original_SP __attribute__ ((section(".noinit")));
#else
#error Unsupported compiler.
#endif

// Function Pototype
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));

// Function Implementation
void wdt_init(void)
{
  // remember reset cause
  reset_cause = reset_cause_get_causes();

  MCUSR = 0;
  wdt_enable_interrupt(WDTO_8S);
  original_SP = SP;
}


void wdt_send_char(char c)
{
  while ((UCSR0A & (1 << UDRE0)) == 0) {};
  UDR0 = c;
}

FILE wdt_str;


static char wd_buff[256];

#define WD_PRINTF(format, ...)		{int cnt = snprintf_P(wd_buff, sizeof(wd_buff), (const char*)F(format), ## __VA_ARGS__); \
  for(int idx = 0; idx < cnt; idx++){wdt_send_char(wd_buff[idx]);}}
  
ISR(WDT_vect, ISR_NAKED)
//ISR(WDT_vect)
{
  uint16_t  top = SP;
  uint16_t  bottom = original_SP;
  uint8_t   tmp_idx = 0;
  uint8_t*  current_sp;

  // disable uart interrupts
  UCSR0B &= ~((1 << RXCIE0) | (1 << TXCIE0));
  
  wdt_enable_interrupt(WDTO_8S);
 
  // watch dog occurs - dump current callstack
  WD_PRINTF("---WatchDOG-START---\n");
  WD_PRINTF("SP top = 0x%04x\n", (uint16_t)top);
  current_sp = (uint8_t*)top;
  current_sp++;
  while (top != bottom)
  {
    WD_PRINTF("%02x", (uint16_t)*current_sp);
    if (tmp_idx == 16)
    {
      WD_PRINTF("\n");
      tmp_idx = 0;
    }
    else
    {
      WD_PRINTF(" ");
      tmp_idx++;
    }
    top++;
    current_sp++;
  }
  WD_PRINTF("\n");
  WD_PRINTF("SP bottom = 0x%04x\n", (uint16_t)bottom);
  WD_PRINTF("---WatchDOG-END---\n");
  
  // dump callstack
  
  wdt_enable_interrupt(WDTO_250MS);
  while (1) {};
} 