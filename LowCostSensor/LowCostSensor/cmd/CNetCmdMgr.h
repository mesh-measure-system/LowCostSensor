/*
 * CNetCmdMgr.h
 *
 *  Created on: 8 lut 2014
 *      Author: Przemek Kieszkowski
 */

#ifndef CNETCMDMGR_H_
#define CNETCMDMGR_H_

#include "CRun.h"
#include "CStream.h"
#include "CNetMgr.h"
#include "mac.h"


#define NET_COMMAND_ENDPOINT_ID             5
#define NET_COMMAND_RESPONSE_ENDPOINT_ID    6

#define NET_COMMAND_ACK_OK                  0
#define NET_COMMAND_ACK_BUSY                1

#define NETC_MAX_ADDRESS_LEN                10

#define CNET_PRINTF_BUFF_SIZE   CNET_MAX_PAYLOAD_SIZE
#define CNET_CMD_MAX_CMD_LEN    (CNET_MAX_PAYLOAD_SIZE)

class CNetCmdMgr : public CRun, public CStream, public CNetPortReceived, public CNetSendConfirm
{
  private:

    // response to send
    char    print_buff[CNET_PRINTF_BUFF_SIZE];
    char    out_buff[CNET_CMD_MAX_CMD_LEN];
    uint8_t out_buff_len;
    bool    wait_to_send_confirm;

    // command from network
    bool			      net_data_available;
	  cnet_frame_data	frame_info;
	  char			      new_net_data[CNET_CMD_MAX_CMD_LEN];
	  uint8_t			    new_net_data_len;

    void  writeToBuff(char c);
    void  toQuotedPrintable(char inChar);

  public:
    void Init();
    virtual void run();
    virtual bool isIdle();

    virtual char* getPrintfBuffer();
    virtual int   getPrintfBufferSize();
    virtual void  write(char c);

    virtual uint16_t getRemainingOutBuffSize();
    virtual bool isOutBuffEmpty();

    virtual bool flushStream();

	  virtual void portData(cnet_frame_data* frame_data_ptr, uint8_t data_len, uint8_t* data_ptr);

    virtual void dataDelivered(uint8_t status, int32_t data_id);
    virtual const char* getClassName() {return __PRETTY_FUNCTION__;}

    virtual ~CNetCmdMgr() {};
};

extern CNetCmdMgr netCmdMgr;

#endif /* CNETCMDMGR_H_ */
