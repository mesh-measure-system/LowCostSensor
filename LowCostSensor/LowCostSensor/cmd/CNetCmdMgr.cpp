/*
 * CNetCmdMgr.cpp
 *
 *  Created on: 8 lut 2014
 *      Author: Przemek Kieszkowski
 */

#include <string.h>

#include "CNetCmdMgr.h"
#include "CCommandsMgr.h"
#include "halUart.h"
#include "CNetwork.h"
#include "macsc_megarf.h"

#include "trace.h"

CNetCmdMgr netCmdMgr;

class CNetResponse:  public CNetPortReceived
{
	virtual void portData(cnet_frame_data* frame_data_ptr, uint8_t data_len, uint8_t* data_ptr)
	{
		// received response from network command
		uint8_t idx;
		// process ind->size bytes of the data pointed by ind->data

//		TRACE_USER("Received response for command\n");
		for (idx = 0; idx < data_len; idx++)
		{
			HAL_UartWriteByte(data_ptr[idx]);
		}
		// 			NWK_SetAckControl(NET_COMMAND_ACK_OK);
	}
};

CNetResponse netRes;

void CNetCmdMgr::Init()
{
  CRun::Init();

  asciiQP = true;
  
  out_buff_len    = 0;
  net_data_available = false;

  wait_to_send_confirm = false;

  // register for the network socket to get command
  netMgr.RegisterPort(NET_COMMAND_ENDPOINT_ID, this);
  netMgr.RegisterPort(NET_COMMAND_RESPONSE_ENDPOINT_ID, &netRes);
}

void CNetCmdMgr::portData(cnet_frame_data* frame_data_ptr, uint8_t data_len, uint8_t* data_ptr)
{
	switch (frame_data_ptr->dstEndPoint)
	{
		case NET_COMMAND_ENDPOINT_ID:
		{
			if (!net_data_available)
			{
				netCmdMgr.net_data_available = true;
				frame_info = *frame_data_ptr;

				if (data_len > sizeof(new_net_data))
				{
					data_len = sizeof(new_net_data);
				}
				memcpy(&new_net_data, data_ptr, data_len);
				new_net_data_len = data_len;

				// TODO command copied - send ack
				//NWK_SetAckControl(NET_COMMAND_ACK_OK);
			}
			else
			{
				// cannot parse command - I am busy
				//		NWK_SetAckControl(NET_COMMAND_ACK_BUSY);
			}
			break;
		}
	}
}

void CNetCmdMgr::run()
{
  if (net_data_available)
  {
    char net_cmd[CNET_CMD_MAX_CMD_LEN + 1];
    // process the frame
    // copy command from network
    memcpy(net_cmd, new_net_data, new_net_data_len);
    net_cmd[new_net_data_len] = 0;

//    TRACE_USER("Received command len = %d, %s\n", new_net_data_len, net_cmd);
    out_buff_len = 0;

    // function should be called to parse commands
    if (cmdMgr.exec_commands(net_cmd, this))
    {
      // command parsed successfully
      if (out_buff_len > 0)
      {
        // sending back command results
        flushStream();
      }
      // after completing command, print a new prompt
    }
    else
    {
      TRACE_ERROR("Command execution error\n");
      // TODO send back error
    }
    net_data_available = false;
  }
}

bool CNetCmdMgr::isIdle()
{
  return !net_data_available;
}

char* CNetCmdMgr::getPrintfBuffer()
{
  return print_buff;
}

int   CNetCmdMgr::getPrintfBufferSize()
{
  return sizeof(print_buff);
}

char to_hex[] = "0123456789ABCDEF";

void CNetCmdMgr::toQuotedPrintable(char inChar)
{
  if ((inChar >= 32) && (inChar <= 126) && (inChar != '='))
  {
    writeToBuff(inChar);
  }
  else
  {
    writeToBuff('=');
    writeToBuff(to_hex[((inChar & 0xF0) >> 4)]);
    writeToBuff(to_hex[(inChar & 0x0F)]);
  }
}
//All printable ASCII characters (decimal values between 33 and 126) may be represented by themselves, except "=" (decimal 61).

void  CNetCmdMgr::write(char c)
{
  if (asciiQP)
  {
    if (out_buff_len == 0)
    {
      // add start character
      writeToBuff('=');
      writeToBuff('?');
    }
    toQuotedPrintable(c);
  }
  else
  {
    writeToBuff(c);
  }
}

void  CNetCmdMgr::writeToBuff(char c)
{
  if (out_buff_len < sizeof(out_buff))
  {
    out_buff[out_buff_len] = c;
    out_buff_len++;
  }
}

// for ascii quoted printable
// start sequence         =?  2
#define ASCII_QP_START    2
// maximum character len      3
#define ASCII_QP_CHAR_LEN 3
// end sequence           ?=  2
#define ASCII_QP_END      2
// new line for end       \n  1
#define ASCII_QP_NEW_LINE 1

#define ASCII_QP_MARGIN   (ASCII_QP_CHAR_LEN + ASCII_QP_END + ASCII_QP_NEW_LINE)

uint16_t CNetCmdMgr::getRemainingOutBuffSize()
{
  int16_t ret_size;
  if (asciiQP)
  {
    ret_size = (sizeof(out_buff) - (ASCII_QP_START + ASCII_QP_END + ASCII_QP_NEW_LINE)) - out_buff_len;
    if ((ret_size < ASCII_QP_CHAR_LEN) || (ret_size > (int16_t)sizeof(out_buff)))
    {
      ret_size = 0;
    }
  }
  else
  {
    ret_size = sizeof(out_buff) - out_buff_len;
  }
  return ret_size;
}

bool CNetCmdMgr::isOutBuffEmpty()
{
  if ((out_buff_len == 0) &&
      (wait_to_send_confirm == false))
  {
    return true;
  }
  return false;
}

void CNetCmdMgr::dataDelivered(uint8_t status, int32_t data_id)
{
  if (status == 0)
  {
    wait_to_send_confirm = false;
    out_buff_len = 0;
    endStream = false;
    breakStream = false;
  }
  else
  {
    uint8_t ctrl = (endStream ? CNET_FH_CTRL_END_COMMAND : 0) | (breakStream ? CNET_FH_CTRL_BREAK_COMMAND : 0) | (asciiQP ? CNET_FH_CTRL_ASCII_COMMAND : 0);
    // error status - resend command
    netMgr.SendData(this, 1, frame_info.SrcAddrSpec.Addr.short_address, frame_info.srcEndPoint, NET_COMMAND_ENDPOINT_ID, ctrl, out_buff_len, (uint8_t*)out_buff);
  }
}

bool CNetCmdMgr::flushStream()
{
  if (wait_to_send_confirm)
  {
    if (breakStream)
    {
      uint8_t ctrl = (endStream ? CNET_FH_CTRL_END_COMMAND : 0) | (breakStream ? CNET_FH_CTRL_BREAK_COMMAND : 0) | (asciiQP ? CNET_FH_CTRL_ASCII_COMMAND : 0);
      wait_to_send_confirm = true;
      netMgr.SendData(this, 2, frame_info.SrcAddrSpec.Addr.short_address, frame_info.srcEndPoint, NET_COMMAND_ENDPOINT_ID, ctrl, 0, (uint8_t*)out_buff);
    }
    return false;
  }

  if (asciiQP)
  {
    if (out_buff_len == 0)
    {
      // add start character
      writeToBuff('=');
      writeToBuff('?');
    }
    // add end character
    writeToBuff('?');
    writeToBuff('=');
  }

  uint8_t ctrl = (endStream ? CNET_FH_CTRL_END_COMMAND : 0) | (breakStream ? CNET_FH_CTRL_BREAK_COMMAND : 0) | (asciiQP ? CNET_FH_CTRL_ASCII_COMMAND : 0);
  wait_to_send_confirm = true;
  netMgr.SendData(this, 1, frame_info.SrcAddrSpec.Addr.short_address, frame_info.srcEndPoint, NET_COMMAND_ENDPOINT_ID, ctrl, out_buff_len, (uint8_t*)out_buff);
  return true;
}

