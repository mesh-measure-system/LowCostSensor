/*
* CLogMgr.cpp
*
* Created: 2014-12-01 12:11:51
* Author: Przemek
*/


#include "CLogMgr.h"
#include "compiler.h"
#include "CFatSDMain.h"
#include "time.h"
#include "ff.h"
#include "CCommandsMgr.h"
#include <string.h>


#include "trace.h"

CLogMgr logMgr;
CFSCleanUpMgr fsCleanUpMgr;

// default constructor
CLogMgr::CLogMgr()
{
} //CLogMgr

void CLogMgr::Init()
{
  FRESULT res;
  CRun::Init();

  file_name_buff[0] = 0;  
  file_ptr = NULL;

  check_file_delay.Init((char*)"CLOG_CHECK_FD", 30 * 1000L);  // 30 seconds
  max_log_file_size.Init((char*)"CLOG_MAX_FSIZE", 10*1024); // 10kB
  
  check_file.mode = SYS_TIMER_INTERVAL_MODE;
  check_file.interval = check_file_delay.get();

  // check if directory for measurements exists
  res = f_mkdir(CLOGMGR_STORE_DIRECTORY_GLOBAL);
  if (!((res == FR_OK) ||
       (res == FR_EXIST)))
  {
    TRACE_ERROR("Directory (%s) not created %d\n", CLOGMGR_STORE_DIRECTORY_GLOBAL, res);
  }
  
#if 1
  // check the date of the last written file to set new date
  res = f_open(&storeFileTime, CLOGMGR_STORE_FILE_TIME, FA_OPEN_ALWAYS | FA_READ);
  if (res == FR_OK)
  {
    char      tmp_buff[(2 * sizeof(CLOGMGR_STORE_FILE_PATTERN)) - 5];
    DWORD     file_size = f_size(&storeFileTime);
    uint16_t  file_read;    // number of the bytes which are read
    
//    TRACE_USER("file '"CLOGMGR_STORE_FILE_TIME"' size %lu\n", file_size);
    if (file_size > (sizeof(tmp_buff) - 1))
    {
      /* Move to end of the file minus bufer size */
      f_lseek(&storeFileTime, f_size(&storeFileTime) - (sizeof(tmp_buff) - 1));
    }

    res = f_read(&storeFileTime, tmp_buff, sizeof(tmp_buff) - 1, &file_read);
    tmp_buff[file_read] = 0;
    if (file_read > 0)
    {
      char* last_file_name;

      // search for the last line
      if (tmp_buff[file_read - 1] == '\n')
      {
        tmp_buff[file_read - 1] = 0;
      }
      last_file_name = strrchr(tmp_buff, '\n');
      if (last_file_name != NULL)
      {
        last_file_name++;
        TRACE_USER("last file name %s\n", last_file_name);
      
        // get date of this file
        FILINFO fno;
        fno.lfname = NULL;
        res = f_stat(last_file_name, &fno);
        if (res == FR_OK)
        {
#if 1
            TRACE_USER("Dir entry: '%s'\n", last_file_name);
            TRACE_USER("     Size: %lu\n", fno.fsize);
            TRACE_USER("     Timestamp: %u/%02u/%02u, %02u:%02u:%02u\n",
                        (fno.fdate >> 9) + 1980, fno.fdate >> 5 & 15, fno.fdate & 31,
                        fno.ftime >> 11, fno.ftime >> 5 & 63, (fno.ftime & 31) * 2);
            TRACE_USER("     Attributes: %c%c%c%c%c\n",
               (fno.fattrib & AM_DIR) ? 'D' : '-',
               (fno.fattrib & AM_RDO) ? 'R' : '-',
               (fno.fattrib & AM_HID) ? 'H' : '-',
               (fno.fattrib & AM_SYS) ? 'S' : '-',
               (fno.fattrib & AM_ARC) ? 'A' : '-');
#endif               
          // make time
          struct tm rtc_time;

          rtc_time.tm_isdst = 0;
          rtc_time.tm_year  = (fno.fdate >> 9) + 1980 - 1900;
          rtc_time.tm_mon   = ((fno.fdate >> 5) & 15) - 1;
          rtc_time.tm_mday  = fno.fdate & 31;
          rtc_time.tm_hour  = fno.ftime >> 11;
          rtc_time.tm_min   = fno.ftime >> 5 & 63;
          rtc_time.tm_sec   = (fno.ftime & 31) * 2;

          time_t new_time = mktime(&rtc_time);

          if ((new_time != (time_t)(-1)) &&
              (new_time > time(NULL)))
          {
            set_system_time(new_time);           
            TRACE_USER("Time from file (%lu) is better then current (%lu) - set it\n", new_time, time(NULL));
          }
          else
          {
            TRACE_USER("Time from file (%lu) is older then current (%lu) - skip to set\n", new_time, time(NULL));
          }                      
        }
      }
    }
    else
    {
      TRACE_USER("Empty file %s\n", CLOGMGR_STORE_FILE_TIME);
    }
  }
  res = f_close(&storeFileTime);
#endif  
  res = f_open(&storeFileTime, CLOGMGR_STORE_FILE_TIME, FA_OPEN_ALWAYS | FA_WRITE);
  f_lseek(&storeFileTime, f_size(&storeFileTime));

}

FIL* CLogMgr::getStoreFileHandle()
{
  if (file_ptr == NULL)
  {
    FRESULT fr;    /* FatFs return code */

    // create file
    // prepare date
    time_t curtime;
    struct tm *loctime;

    /* Get the current time. */
    curtime = time (NULL);

    /* Convert it to local time representation. */
    loctime = localtime (&curtime);

    /* Print it out in a nice format. */
    strftime(dir_name_buff, sizeof(dir_name_buff), CLOGMGR_STORE_DIRECTORY_LOCAL, loctime);
    fr = f_mkdir(dir_name_buff);
    if (fr != FR_OK)
    {
      TRACE_ERROR("Error (%d) creating directory %s\n", fr, dir_name_buff);
    }
    
    strftime(file_name_buff, sizeof(file_name_buff), CLOGMGR_STORE_FILE_PATTERN, loctime);
    // open the file
    fr = f_open(&storeFile, file_name_buff, FA_OPEN_ALWAYS | FA_WRITE);
    if (fr != FR_OK)
    {
      TRACE_ERROR("Error creating file '%s', fr=%d\n", file_name_buff, (int)fr);
    }
    else
    {
      // seek to end
      fr = f_lseek(&storeFile, f_size(&storeFile));
      file_ptr = &storeFile;      
      f_sync(file_ptr);
      TRACE_USER("New file name '%s' open status = %d\n", file_name_buff, fr);
    }
    
    // add file name to last-file.txt
    f_printf(&storeFileTime, "%s\n", file_name_buff);
    f_sync(&storeFileTime);
  }

  // start timer to flush/close file
  if (SYS_TimerStarted(&check_file))
  {
    SYS_TimerStop(&check_file);
  }
  SYS_TimerStart(&check_file, this);

  return file_ptr;
}

int CLogMgr::store(const char* format, ...)
{
  uint16_t  written;
  uint16_t  real_wr;
  va_list   args;
  int       date_size;
  FRESULT   fres;

  // prepare date
  time_t curtime;
  struct tm *loctime;

  /* Get the current time. */
  curtime = time (NULL);

  /* Convert it to local time representation. */
  loctime = localtime (&curtime);

  /* Print it out in a nice format. */
  date_size = strftime(store_buff, sizeof(store_buff), "%F, %X ", loctime);

  
  va_start (args, format);
  written = vsnprintf(store_buff + date_size - 1, sizeof(store_buff) - date_size, format, args);
  va_end (args);
  
  written += date_size;

  printf(store_buff);
  fres = f_write(getStoreFileHandle(), store_buff, written, &real_wr);
  if (fres != FR_OK)
  {
    TRACE_ERROR("Problem with store data in file '%s' ferr = %d\n", file_name_buff, fres);
  }
  
  return written;
}

int CLogMgr::store(const __FlashStringHelper* format, ...)
{
  uint16_t  written;
  uint16_t  real_wr;
  va_list   args;
  int       date_size;
  FRESULT   fres;

  // prepare date
  time_t curtime;
  struct tm *loctime;

  /* Get the current time. */
  curtime = time (NULL);

  /* Convert it to local time representation. */
  loctime = localtime (&curtime);

  /* Print it out in a nice format. */
  date_size = strftime(store_buff, sizeof(store_buff), "%F, %X ", loctime);

  va_start (args, format);
  written = vsnprintf_P(store_buff + date_size - 1, sizeof(store_buff) - date_size, (const char *)format, args);
  va_end (args);
  
  written += date_size;

  printf(store_buff);
  fres = f_write(getStoreFileHandle(), store_buff, written, &real_wr);
  if (fres != FR_OK)
  {
    TRACE_ERROR("Problem with store data in file '%s' ferr = %d\n", file_name_buff, fres);
  }
  
  return written;
}

void CLogMgr::timerFired(struct SYS_Timer_t* timer)
{
  UNUSED(timer);
  // perform free disk check
  setState(CHECK_FILE);
}

void CLogMgr::setState(logMgrState_t new_state)
{
  switch (new_state)
  {
    case IDLE:
    {
      state = IDLE;
      break;
    }
    case CHECK_FILE:
    {
      state = new_state;
      break;
    }
  }    
}

void CLogMgr::run()
{
  switch(state)
  {
    case IDLE:
    {
      break;
    }
    case CHECK_FILE:
    {
      if (file_ptr != NULL)
      {
        // flush file cache
        f_sync(file_ptr);
        
        DWORD file_size = f_size(file_ptr);
//        TRACE_USER("File size = %lu\n", file_size);
        
        if (file_size > max_log_file_size.get())
        {
          // file is to big - close it
          f_close(file_ptr);
          file_ptr = NULL;
          file_name_buff[0] = 0;
          TRACE_USER("File is to big - close it and open new one\n");
        }
      }
      setState(IDLE);
      break;
    }
  }
}

bool CLogMgr::isIdle()
{
  return state == IDLE;
}


// default constructor
CFSCleanUpMgr::CFSCleanUpMgr()
{

}

void CFSCleanUpMgr::Init()
{
  CRun::Init();

  state = IDLE;

  cleanup_period.Init((char*)"CLOG_CLEANUP_P", 10*60*1000L);

  // setup timer for periodic check for of the available free volume
  check_timer.mode = SYS_TIMER_PERIODIC_MODE;
  check_timer.interval = cleanup_period.get();
  SYS_TimerStart(&check_timer, this);

  // perform free disk check at startup
  setState(CHECK_FREE_PLACE);
}

void CFSCleanUpMgr::timerFired(struct SYS_Timer_t* timer)
{
  UNUSED(timer);
  TRACE_USER("Cleanup timer fired\n");

  // perform free disk check
  setState(CHECK_FREE_PLACE);
}

void CFSCleanUpMgr::setState(cleanUpState_t new_state)
{
  switch (new_state)
  {
    case IDLE:
    {
      state = IDLE;
      break;
    }
    case CHECK_FREE_PLACE:
    {
      state = new_state;
      break;
    }
    case REMOVE_OLD_FILES:
    {
      state = new_state;
      break;
    }
  }
}

void CFSCleanUpMgr::run()
{
  switch (state)
  {
    case IDLE:
    {
      break;
    }
    case CHECK_FREE_PLACE:
    {
      FRESULT res;
      FATFS*  fs = &MemoryFatFs;
      DWORD   fre_clust, fre_sect, tot_sect;

      /* Get volume information and free clusters of drive 0 */
      res = f_getfree(CLOGMGR_STORE_DISK, &fre_clust, &fs);
      wdt_reset();
      if (res == FR_OK)
      {
        /* Get total sectors and free sectors */
        tot_sect = (fs->n_fatent - 2) * fs->csize;
        fre_sect = fre_clust * fs->csize;

        /* Print the free space (assuming 512 bytes/sector) */
        TRACE_USER("%10lu KiB total drive space.\n", tot_sect / 2);
        TRACE_USER("%10lu KiB available.\n", fre_sect / 2);
        setState(IDLE);
      }
      else
      {
        TRACE_ERROR("f_getfree returns error %d\n", (int)res);
        setState(IDLE);
      }
      break;
    }
    case REMOVE_OLD_FILES:
    {
      break;
    }
  }
}

bool CFSCleanUpMgr::isIdle()
{
  return (state == IDLE);
}

