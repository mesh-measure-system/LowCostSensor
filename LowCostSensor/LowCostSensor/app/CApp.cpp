/*
 * CApp.cpp
 *
 *  Created on: 07-11-2013
 *      Author: Przemek Kieszkowski
 */

#include "CApp.h"

#include <stdint.h>

#include "CCmdSetting.h"
#include "CCommandsMgr.h"
#include "CNetCmd.h"
#include "CUartMgr.h"
#include "halUart.h"
#include "CADC.h"
#include "CFlashString.h"
#include "CEventMgr.h"
#include "sys.h"
#include "CBattery.h"
#include "CLedMgr.h"
#include "CPowerMgr.h"
#include "CStratificationMgr.h"
#include "CNetCmdMgr.h"
#include "CToolCommands.h"
#include "CFatSDMain.h"
#include "CNetMgr.h"
#include "CNetwork.h"
#include "CSysWDT.h"
#include "CLogMgr.h"

#include "conf_common_sw_timer.h"
#include "common_sw_timer.h"

#include "trace.h"


// class instance
CApp app;

#define CAPP_LAST_CALL_VALID  (0x87654321)
#if defined (__GNUC__)
volatile char*    last_call_p     __attribute__ ((section(".noinit")));
volatile uint32_t valid_last_call __attribute__ ((section(".noinit")));
#else
#error Unsupported compiler.
#endif

void CApp::addToRun(CRun* run)
{
  run_classes.add_item(run);
}

//#define APP_NAME() ##APPNAME
void CApp::Init()
{
	run_classes.init();

	appState = APP_STATE_INITIAL;

	SYS_Init();

	TRACE_USER("-- Low cost sensor --\n");
	TRACE_USER("-- Compiled: %s %s -:)-\n", __DATE__, __TIME__);

  if (valid_last_call == CAPP_LAST_CALL_VALID)
  {
    TRACE_USER("Last call to %s\n", last_call_p);
  }

	fatSDMgr.Init();

	settingsMgr.Init();

  wdt_reset();
	eventMgr.Init();
	uartMgr.Init();
	adc.Init();

	cmdMgr.Init();

	sysWDT.Init();

	ledMgr.Init();

  wdt_reset();
  nwk.Init();
  netMgr.Init();



  wdt_reset();
  powerMgr.Init();
  battMgr.Init();
  stratificationMgr.Init();

  logMgr.Init();
  fsCleanUpMgr.Init();

  netCmdMgr.Init();


  // commands registration
  toolCmd.Init();
  cmdSettingList.init((char*)CMD_SETTING_LIST_NAME);
  cmdSettingSet.init((char*)CMD_SETTING_SET_NAME);
  netCmd.init((char*)"net");

  fatSDMgr.InitCommands();

}

void CApp::TaskHandler()
{
  switch (appState)
  {
    case APP_STATE_INITIAL:
    {
      appState = APP_STATE_STARTED;
    }
    break;

    case APP_STATE_STARTED:
    {
    }
    break;
  }
}


void CApp::Run()
{
  while (1)
  {
    last_call_p = (char*)"SYS_TaskHandler";
    SYS_TaskHandler();
    last_call_p = (char*)"HAL_UartTaskHandler";
    HAL_UartTaskHandler();
    last_call_p = (char*)"TaskHandler";
    TaskHandler();

    // run method from classes
    CListIterator<CRun> run_iter(&run_classes);
    while(run_iter.get())
    {
      valid_last_call = CAPP_LAST_CALL_VALID;
      last_call_p = (char*)run_iter.get()->getClassName();
      run_iter.get()->run();
      run_iter.next();
    }
  }
}
