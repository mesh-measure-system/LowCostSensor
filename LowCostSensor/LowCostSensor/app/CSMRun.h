/* 
* CSMRun.h
*
* Created: 2017-04-05 19:24:18
* Author: przemek
*/


#ifndef __CSMRUN_H__
#define __CSMRUN_H__

#include "CRun.h"

class CSMRun : public CRun
{
//functions
public:
	virtual ~CSMRun();
	virtual void Method1()=0;//make CSMRun not instantiable
	virtual void Method2();
	void Method3();

}; //CSMRun

#endif //__CSMRUN_H__
