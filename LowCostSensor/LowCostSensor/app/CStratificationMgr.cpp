/*
 * CStratificationMgr.cpp
 *
 *  Created on: 16-11-2013
 *      Author: Przemek Kieszkowski
 */

#include <string.h>
#include "CStratificationMgr.h"
#include "CCommandsMgr.h"
#include "CLedMgr.h"
#include "CNetMgr.h"
#include "CNetCmdMgr.h"
#include "CLogMgr.h"

#include "time.h"

#include "trace.h"

class CStratifyCommand : public CCommand
{
  public:
    virtual bool start_command(CStream* stream, char* argv[], int argc);
    virtual bool is_idle() {return true;};
    virtual void print_help(CStream* stream) {stream->printf(F("Displays stratify value\n"));};
    virtual ~CStratifyCommand() {};
};

bool CStratifyCommand::start_command(CStream* stream, char* argv[], int argc)
{
  UNUSED(argv);
  UNUSED(argc);
  stream->printf("Stratification %5.2f mm\n", stratificationMgr.stratification.getStratification());
  return false;
}


CStratifyCommand    stratifyCmd;
CStratificationMgr  stratificationMgr;

// returns stratification in the 0.1 um resolution
#define ST_MAX_SCALE_IN_01UM  10.000f
#define MAX_AD_RESULT         102.3f

double  CStratification::getStratification()
{
  uint32_t result;
  double converted;
  result = getConversionData();

//  TRACE_USER("Conversion %d \n", (uint16_t)result);
  converted = (ST_MAX_SCALE_IN_01UM * result)/ MAX_AD_RESULT;
  return converted;
}

void CStratification::Init(ADCChannel_t chann, HAL_AdcVoltageReference_t vref)
{
  CADCdata::Init(chann, vref);
  adc_power.Init(CPin::PIN_PORTD, 4, CPin::PIN_OUT);
  adc_power.Set();
  data = 0.0f;
}


void CStratification::conversionFinished()
{
	if (data != getStratification())
	{
		data = getStratification();
		stratificationMgr.setState(CStratificationMgr::STRATIFICATION_CHANGED);
	}
	else
	{
		stratificationMgr.setState(CStratificationMgr::MEASURED);
	}
}

void CStratificationMgr::Init()
{
  CRun::Init();
  stratification.Init(STRATIFICATION, AVCC);

  stratifyCmd.init((char*)"stratify");

  power_enable_time.Init((char*)"STR_POW_EN", 40);
  fast_measure_sleep.Init((char*)"STR_FAST_SLEEP", 330);
  fast_measure_period.Init((char*)"STR_FAST_PERIOD", 45*1000L);
  slow_measure_sleep.Init((char*)"STR_SLOW_SLEEP", 5 * 1000);
  alarm_level_um.Init((char*)"ALARM_LEVEL_UM", 51 * 1000L * 1000L);


  // setup timer for periodic stratification voltage measurement
  timer.mode = SYS_TIMER_INTERVAL_MODE;
  timer.interval = slow_measure_sleep.get();

  pow_en_timer.mode = SYS_TIMER_INTERVAL_MODE;
  pow_en_timer.handler =  NULL;

  fast_measure_period_timer.mode      = SYS_TIMER_INTERVAL_MODE;
  fast_measure_period_timer.interval  = fast_measure_period.get();

  setState(IDLE);
}

void CStratificationMgr::dataDelivered(uint8_t status, int32_t data_id)
{
	if (status != 225)	// PK remove this if
	{
		TRACE_USER("alarm delivered status %d\n", (int)status);
	}
}


void CStratificationMgr::setState(strState_t new_state)
{
  state = new_state;
  switch (new_state)
  {
    case IDLE:
    {
//      SYS_TimerStart(&timer, this);
      break;
    }
    case ENABLE_SENSOR_POWER:
    {
      stratification.adc_power.Clr();
      SYS_TimerStart(&pow_en_timer, this);
      break;
    }
    case START_MEASURE:
    {
      if (!stratification.startConversion())
      {
        TRACE_ERROR("Stratify conv start problem\n");
		    setState(IDLE);
      }
      break;
    }
	case STRATIFICATION_CHANGED:
	{
		double str = stratification.getStratification();

    // speedup measurements
    timer.interval = fast_measure_sleep.get();    // enable fast timer
    SYS_TimerStart(&fast_measure_period_timer, this);

//		TRACE_USER("Stratification %5.2f mm\n", stratification.getStratification());
		if (str >= ((float)alarm_level_um.get() / 1000000.0f))
		{
			ledMgr.ledRed.Play(&ledMgr.alarmSq);
			// TODO send broadcast alarm frame
		  {
			  uint8_t buff[30];
			  sprintf((char*)buff, "alarm");
//           	  bool SendData(CNetSendConfirm* confirm_p, int32_t data_id, uint16_t short_addr, uint8_t dstEndPoint, uint8_t srcEndPoint, uint8_t ctrl, uint8_t data_len, uint8_t* data_ptr);
//			  if (!netMgr.SendData(this, 345, 0xFFFF, NET_COMMAND_ENDPOINT_ID, NET_COMMAND_RESPONSE_ENDPOINT_ID, CNET_FH_CTRL_END_COMMAND | CNET_FH_CTRL_ASCII_COMMAND, strlen((char*)buff), buff))
        if (!netMgr.SendBroadcast(this, 345, NET_COMMAND_ENDPOINT_ID, NET_COMMAND_RESPONSE_ENDPOINT_ID, strlen((char*)buff), buff))
			  {
				  TRACE_ERROR("Transmission error\n");
			  }

		  }
		}
    logMgr.store("Rozw: %5.2f mm\n", str);
		setState(MEASURED);
		break;
	}
    case MEASURED:
    {
      stratification.adc_power.Set();
      break;
    }
  }
}

void CStratificationMgr::timerFired(struct SYS_Timer_t* timer_fired)
{
  if (timer_fired == &timer)
  {
    setState(ENABLE_SENSOR_POWER);
  }
  else if (timer_fired == &pow_en_timer)
  {
    setState(START_MEASURE);
  }
  else if (timer_fired == &fast_measure_period_timer)
  {
    timer.interval = slow_measure_sleep.get();  // enable slow timer
  }
}

void CStratificationMgr::run()
{
  switch (state)
  {
//     case IDLE:
//     case ENABLE_SENSOR_POWER:
//     case START_MEASURE:
    case MEASURED:
    {
      setState(IDLE);
      break;
    }
	  default:
	  {
		  break;
	  }
  }
}

bool CStratificationMgr::isIdle()
{
  return state == IDLE;
}

