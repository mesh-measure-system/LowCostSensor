/*
 * CPowerMgr.cpp
 *
 *  Created on: 07-11-2013
 *      Author: Przemek Kieszkowski
 */

#include <stdlib.h>

#include "CPowerMgr.h"
#include "CApp.h"
#include "trace.h"
#include "sysTimer.h"
#include "halUart.h"

// class instance
CPowerMgr powerMgr;

void CPowerMgr::Init()
{
  CRun::Init();
}

extern  volatile bool txRunning;

void CPowerMgr::run()
{
#if 0   // nothing to do now
  bool idle = true;
  // check if all registered modules are in the idle state
  CListIterator<CRun> run_iter(&app.run_classes);
  while(run_iter.get())
  {
    idle = idle && run_iter.get()->isIdle();
    run_iter.next();
  }
  if (idle)
  {
    uint32_t  nextFire;
    // all classes are in idle - we can go to low power
    // check timer for length of the low power state

    nextFire = SYS_TimerGetNextFireDelay();
    if (nextFire > HAL_RTC_IDLE_SLEEP_MARGIN)
    {
      TRACE_USER("Next fire %lu\n", nextFire);
      HAL_UartWriteFlush();
      HAL_RTCIdleSleep(nextFire);
    }
  }
#endif
}

bool CPowerMgr::isIdle()
{
  return true;
}
