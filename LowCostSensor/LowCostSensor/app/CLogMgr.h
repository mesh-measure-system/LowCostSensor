/*
 * CLogMgr.h
 *
 * Created: 2014-12-01 12:11:51
 * Author: Przemek
 */


#ifndef __CLOGMGR_H__
#define __CLOGMGR_H__

#include "CRun.h"
#include "sysTimer.h"
#include "CSettingsMgr.h"
#include "CFlashString.h"
#include "ff.h"


#define CLOGMGR_STORE_DISK                "0:"
#define CLOGMGR_STORE_DIRECTORY_GLOBAL    CLOGMGR_STORE_DISK "pomiary"
#define CLOGMGR_STORE_DIRECTORY_LOCAL     CLOGMGR_STORE_DIRECTORY_GLOBAL "/%Y-%m"
#define CLOGMGR_STORE_FILE_PATTERN        CLOGMGR_STORE_DIRECTORY_LOCAL "/%F-%H-%M-%S-log.txt"
#define CLOGMGR_STORE_FILE_TIME           CLOGMGR_STORE_DIRECTORY_GLOBAL "/ostatni-plik.txt"

class CLogMgr : public CRun, public CTimerFired
{
//variables
public:
protected:
  SYS_Timer_t check_file;
  CSetting<uint32_t, SETTING_UINT32> check_file_delay;
  CSetting<uint32_t, SETTING_UINT32> max_log_file_size;
  
private:
  char store_buff[512];
  
  // functions for file operation
  char  dir_name_buff[64];
  char  file_name_buff[64];
  FIL   storeFile;      /* File object needed for each open file */
  FIL*  file_ptr;       // file_ptr is not null when file is open

  FIL   storeFileTime;  // file for store name of the last written data file

//functions
public:
  enum logMgrState_t {IDLE, CHECK_FILE};
  logMgrState_t state;

//  enum CLogClass();
  CLogMgr();

  void Init();

  virtual void run();
  virtual bool isIdle();

  int store(const char* format, ...);
  int store(const __FlashStringHelper* format, ...);
  
  virtual void timerFired(struct SYS_Timer_t* timer);

  void setState(logMgrState_t new_state);
  virtual const char* getClassName() {return __PRETTY_FUNCTION__;}

	~CLogMgr() {};
protected:
  FIL* getStoreFileHandle();
  
private:
	CLogMgr( const CLogMgr &c );
	CLogMgr& operator=( const CLogMgr &c );

}; //CLogMgr

class CFSCleanUpMgr : public CRun, public CTimerFired
{
//variables
public:
  enum cleanUpState_t {IDLE, CHECK_FREE_PLACE, REMOVE_OLD_FILES};
  cleanUpState_t state;

protected:
  SYS_Timer_t check_timer;
  CSetting<uint32_t, SETTING_UINT32> cleanup_period;

private:

//functions
public:
	CFSCleanUpMgr();

  void Init();

  virtual void run();
  virtual bool isIdle();
  virtual void timerFired(struct SYS_Timer_t* timer);
  virtual const char* getClassName() {return __PRETTY_FUNCTION__;}

  void setState(cleanUpState_t new_state);
  ~CFSCleanUpMgr() {};
};

extern CLogMgr logMgr;
extern CFSCleanUpMgr fsCleanUpMgr;

#endif //__CLOGMGR_H__
